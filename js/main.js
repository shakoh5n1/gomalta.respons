$( function() {
    var dateFormat = "dd m y",
      from = $( "#from" )
        .datepicker({
          dateFormat: "dd M y" ,
          // defaultDate: "+1w",
          changeYear: true,
          changeMonth: true
          
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
        
      to = $( "#to" ).datepicker({
        dateFormat: "dd M y" ,
        // defaultDate: "+1w",
        changeYear: true,
        changeMonth: true
      
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
  
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
  
      return date;
    }
  
    $('#from').datepicker('setDate', 'today');
    $('#to').datepicker('setDate', 'today + 1w');

    $( "#datepicker1" ).datepicker({
      dateFormat: "dd M y" ,
      changeYear: true,
        changeMonth: true
    });
    $( "#datepicker2" ).datepicker({
      dateFormat: "dd M y" ,
      changeYear: true,
        changeMonth: true
    });
})

jQuery('<div class="quantity-nav"><img class="quantity-button quantity-up" src="img/arrowupx1.png" alt=""><img class="quantity-button quantity-down" src="img/arrowdown1x.png" alt=""></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
  var spinner = jQuery(this),
    input = spinner.find('input[type="number"]'),
    btnUp = spinner.find('.quantity-up'),
    btnDown = spinner.find('.quantity-down'),
    min = input.attr('min'),
    max = input.attr('max');

  btnUp.click(function() {
    var oldValue = parseFloat(input.val());
    if (oldValue >= max) {
      var newVal = oldValue;
    } else {
      var newVal = oldValue + 1;
    }
    spinner.find("input").val(newVal);
    spinner.find("input").trigger("change");
  });

  btnDown.click(function() {
    var oldValue = parseFloat(input.val());
    if (oldValue <= min) {
      var newVal = oldValue;
    } else {
      var newVal = oldValue - 1;
    }
    spinner.find("input").val(newVal);
    spinner.find("input").trigger("change");
  });




});

var details = document.getElementById('details');
var flightResultDetails = document.getElementById('collapseExample');

if(details) {
  details.addEventListener('click' , function() {
    if(flightResultDetails.className == "collapse" ) {
      
      details.style.backgroundImage = 'url(img/arrowupx1.png)';
      
      
      
    } else {
      
      details.style.backgroundImage = 'url(img/arrowdown1x.png)';
      
      
    }
    
  })
}




function openNav() {
  document.getElementById("mySidepanel").style.width = "80%";
}

function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}



// var changebtn = document.getElementById('change');
// var fromFlight = document.getElementById('fromFlight');

// if(changebtn) {
//   changebtn.addEventListener('click', function() {
//     console.log(fromFlight.placeholder);
//   })
// }


